#!/bin/sh
if [ "${USER_ID}" != 1000 ]; then
	usermod -u ${USER_ID} node
	chown node:node /home/node
fi
if [ "${USER_GID}" != 1000 ]; then
	 groupmod -g ${USER_GID} node
	 chown node:node /home/node
fi

docker-entrypoint.sh "$@"
